# Seti-2
Новые инструменты для работы с курсом сетей, более толковые  
В отличие от прошлого варианта работают через терминал
```sh
$ echo '100' | ./base --from 2
4
```
Пока что не всё переехало, в разработке
Планирую добавить разные полезные утилиты
## Где документация?
Используйте `--help`!  
Если что-то не пробовали - попробуйте позапускать и посмотреть что происходит.
```
$ ./base --help
usage: SETI::base [-h] [-f FROM] [-t TO] [-U] [-I]

Converting number systems

optional arguments:
  -h, --help            show this help message and exit
  -f FROM, --from FROM  Base from which to convert (default 10)
  -t TO, --to TO        Base to which to convert (default 10)
  -U, --upper           Make numbers of base > 10 uppercase
  -I, --ignore          Ignore broken numbers with invalid characters
```
## Что за инструмент такой - `highlight.py`?
Это файл для подсветки ошибок, не трогайте.  
Он подключается разными инструментами.
## Не работает на Windows
Есть два варианта:
 - Перейти на Линукс
 - Писать `python` перед коммандой:
   ```
   $ echo '100' | python ./base --from 2
   ```
