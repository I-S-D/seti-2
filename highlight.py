try:
  from colorama import init as __colorama_init
  from colorama import Fore, Style
except ImportError:
  print("Please install colorama")
  print("It can be done by:")
  print("$ pip install colorama --user")

__colorama_initted = False

# Log line, showing error message at `pos` 
# If line length is `> maxlen`, it will be trimmed to fit
def __log(line, pos, error, maxlen=50, color=None, select=1):
  print('\n' + '-'*maxlen)
  if not __colorama_initted:
    __colorama_init()
  l = 0
  r = 0
  reallen = 0
  mll = max(len(error), select)
  if len(line) > maxlen:
    offset = (maxlen - mll) / 2
    l = max(pos - offset, 0)
    r = min(l + mll, len(line))
    reallen = r - l + 1
  else:
    l = 0
    r = len(line)
    reallen = r
  

  if l != 0:
    print('...', end='')
    reallen -= 3

  nextws = False
  if r != len(line):
    nextws = True
    reallen -= 3

  if color:
    print(line[l:pos] + color + line[pos:pos+select] + Style.RESET_ALL + line[pos+select:r], end='')
  else:
    print(line[l:r], end='')
  if nextws:
    print('...', end='')
  print()
  if color:
    print(color, end='')
  print((pos - l)*" " + "^" + "~"*(select-1) + " " + error)
  if color:
    print(Style.RESET_ALL, end='')

def log(line, pos, error, maxlen=50, select=1):
  __log(line, pos, error, maxlen, select=select)

def err(line, pos, error, maxlen=50, select=1):
  __log(line, pos, error, maxlen, color=Fore.RED, select=select)

def info(line, pos, error, maxlen=50, select=1):
  __log(line, pos, error, maxlen, color=Fore.BLUE, select=select)

def warn(line, pos, error, maxlen=50, select=1):
  __log(line, pos, error, maxlen, color=Fore.YELLOW, select=select)

def debug(line, pos, error, maxlen=50, select=1):
  __log(line, pos, error, maxlen, color=Fore.GREEN, select=select)


if __name__ == '__main__':
  err('while (fail) {}', 7, 'Undefined fail', select=4)
  debug('...Elephants...', 3, 'are flying', select=9)